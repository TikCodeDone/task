import {Routes} from '@angular/router';
import {YouTubeSearchComponent} from './you-tube-search/you-tube-search.component';
import {HistoryComponent} from './history/history.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/search',
    pathMatch: 'full',
  },
  {
    path: 'search',
    component: YouTubeSearchComponent
  },
  {
    path: 'history',
    component: HistoryComponent
  },
];
