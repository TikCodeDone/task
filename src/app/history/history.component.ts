import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../services/local-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  history: any;

  constructor(private localStorageService: LocalStorageService, private router: Router) { }

  ngOnInit() {
    this.history = this.localStorageService.get('history') || [];
  }

  navigateWithParams(item) {
    this.router.navigate(['/search', {category: item.category, text: item.text}]);
  }

}
