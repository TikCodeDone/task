/**
 * SearchResult is a data-structure that holds an individual
 * record from a YouTube video search
 */
export class SearchResult {
  id: string;
  title: string;
  kind: string;
  description: string;
  thumbnailUrl: string;
  videoUrl: string;
  publishedAt: string;

  constructor(obj?: any) {
    this.id              = obj && obj.id             || null;
    this.title           = obj && obj.title          || null;
    this.description     = obj && obj.description    || null;
    this.thumbnailUrl    = obj && obj.thumbnailUrl   || null;
    this.kind            = obj && obj.kind   || null;
    this.publishedAt    = obj && obj.publishedAt   || null;
    this.videoUrl        = obj && obj.videoUrl       ||
                             `https://www.youtube.com/watch?v=${this.id}`;
  }
}
