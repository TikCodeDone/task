import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ElementRef
} from '@angular/core';

// By importing just the rxjs operators we need, We're theoretically able
// to reduce our build size vs. importing all of them.
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switch';

import { YouTubeSearchService } from './you-tube-search.service';
import { SearchResult } from './search-result.model';
import {LocalStorageService} from '../services/local-storage.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() results: EventEmitter<SearchResult[]> = new EventEmitter<SearchResult[]>();

  categories: any;
  selectedCategory: any;
  searchParams = {
    text: '',
    category: ''
  };

  constructor(private youtube: YouTubeSearchService,
              private el: ElementRef, private localStorageService: LocalStorageService, private route: ActivatedRoute) {
    this.categories = [
      {key: '', label:  'Any'},
      {key: 'video', label:  'Video'},
      {key: 'channel', label: 'Channel'},
      {key: 'playlist', label: 'Play List'}
    ];
    this.selectedCategory = this.categories[0];
  }

  ngOnInit(): void {
    // convert the `keyup` event into an observable stream
    // Observable.fromEvent(this.el.nativeElement, 'keyup')
    //   .map((e: any) => e.target.value) // extract the value of the input
    //   .filter((text: string) => text.length > 1) // filter out if empty
    //   .debounceTime(250)                         // only once every 250ms
    //   .do(() => this.loading.emit(true))         // enable loading
    //   // search, discarding old events if new input comes in
    //   .map((query: string) => this.youtube.search(query))
    //   .switch()
    //   // act on the return of the search
    //   .subscribe(
    //     (results: SearchResult[]) => { // on sucesss
    //       this.loading.emit(false);
    //       this.results.emit(results);
    //     },
    //     (err: any) => { // on error
    //       console.log(err);
    //       this.loading.emit(false);
    //     },
    //     () => { // on completion
    //       this.loading.emit(false);
    //     }
    //   );
    const category = this.route.snapshot.paramMap.get('category');
    const text = this.route.snapshot.paramMap.get('text');
    if(category || text) {
      this.selectedCategory = this.categories.filter(x => x.key === category)[0];
      this.searchParams.text = this.route.snapshot.paramMap.get('text');
      this.searchWithData();
    }



  }

  search() {
    if(!this.searchParams.text) {
      return;
    }
    this.searchWithData();
  }
  changeCategory() {
    if(!this.searchParams.text) {
      return;
    }
    this.searchWithData();
  }

  searchWithData() {
    this.searchParams.category = this.selectedCategory.key;
    let history = this.localStorageService.get('history');
    if(!history){
      history = {items: []};
    }
    history.items.push({date: new Date, params: this.searchParams});
    this.localStorageService.set('history', history);
    this.youtube.search(this.searchParams).subscribe(
      (results: SearchResult[]) => { // on success
        this.results.emit(results);
        this.loading.emit(false);
      },
      (err: any) => { // on error
        console.log(err);
      }
    );
  }
}
